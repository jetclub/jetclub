// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JetClubGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JETCLUB_API AJetClubGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
