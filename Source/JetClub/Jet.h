// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "Bullet.h"
#include "Jet.generated.h"

UCLASS()
class JETCLUB_API AJet : public AWheeledVehicle
{
	GENERATED_BODY()
public:
    AJet();
    virtual void Tick(float DeltaTime) override;
    void ApplyThrottle(float AxisValue);
    void ApplySteering(float AxisValue);
    void Roll(float AxisValue);
    void Pitch(float AxisValue);
    void PitchCamera(float AxisValue);
    void YawCamera(float AxisValue);

    void ApplyThrust(float DeltaTime);
    void Thrust();
    void StopThrust();

    /** When true, player wants to jump */
    UPROPERTY(ReplicatedUsing=OnRep_Jump, BlueprintReadOnly, Category=Character)
    uint32 bPressedJump:1;

    UPROPERTY(BlueprintReadOnly, Category=Character)
    bool bPressedThrust;

    UPROPERTY(BlueprintReadOnly, Category=Character)
    bool bCanJump;

    UPROPERTY(BlueprintReadOnly, Category=Character)
    bool bCanThrust;

    UPROPERTY(Replicated, BlueprintReadOnly, Category=Character)
    bool bAnyWheelTouching;

    UPROPERTY(BlueprintReadOnly, Category=Character)
    bool bJetCam;

    /** Tracks whether or not the character was already jumping last frame. */
    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient, Category=Character)
    uint32 bWasJumping : 1;

    /**
     * Jump key Held Time.
     * This is the time that the player has held the jump key, in seconds.
     */
    UPROPERTY(Transient, BlueprintReadOnly, VisibleInstanceOnly, Category=Character)
    float JumpKeyHoldTime;

    /** Amount of jump force time remaining, if JumpMaxHoldTime > 0. */
    UPROPERTY(Transient, BlueprintReadOnly, VisibleInstanceOnly, Category=Character)
    float JumpForceTimeRemaining;

    /** Track last time a jump force started for a proxy. */
    UPROPERTY(Transient, BlueprintReadOnly, VisibleInstanceOnly, Category=Character)
    float ProxyJumpForceStartedTime;

    /**
     * The max time the jump key can be held.
     * Note that if StopJumping() is not called before the max jump hold time is reached,
     * then the character will carry on receiving vertical velocity. Therefore it is usually
     * best to call StopJumping() when jump input has ceased (such as a button up event).
     */
    UPROPERTY(BlueprintReadWrite, Replicated, Category=Action, Meta=(ClampMin=0.0, UIMin=0.0))
    float JumpMaxHoldTime;

    UPROPERTY(BlueprintReadWrite, Replicated, Category=Action)
    float JumpStrength;

    /**
     * The max number of jumps the character can perform.
     * Note that if JumpMaxHoldTime is non zero and StopJumping is not called, the player
     * may be able to perform and unlimited number of jumps. Therefore it is usually
     * best to call StopJumping() when jump input has ceased (such as a button up event).
     */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category=Action)
    int32 JumpMaxCount;

    /**
     * Tracks the current number of jumps performed.
     * This is incremented in CheckJumpInput, used in CanJump_Implementation, and reset in OnMovementModeChanged.
     * When providing overrides for these methods, it's recommended to either manually
     * increment / reset this value, or call the Super:: method.
     */
    UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category=Action)
    int32 JumpCurrentCount;

    /**
     * Make the character jump on the next update.
     * If you want your character to jump according to the time that the jump key is held,
     * then you can set JumpKeyHoldTime to some non-zero value. Make sure in this case to
     * call StopJumping() when you want the jump's z-velocity to stop being applied (such
     * as on a button up event), otherwise the character will carry on receiving the
     * velocity until JumpKeyHoldTime is reached.
     */
    UFUNCTION(Server, Reliable)
    void JumpServer();

    UFUNCTION(BlueprintCallable, Category=Character)
    void OnRep_Jump();

    /**
     * Stop the character from jumping on the next update.
     * Call this from an input event (such as a button 'up' event) to cease applying
     * jump Z-velocity. If this is not called, then jump z-velocity will be applied
     * until JumpMaxHoldTime is reached.
     */
    UFUNCTION(BlueprintCallable, Category=Character)
    virtual void StopJumping();

    UFUNCTION(BlueprintCallable, Category=Character)
    virtual void ReleaseHandbrake();

    UFUNCTION(BlueprintCallable, Category=Character)
    virtual void Handbrake();

    UFUNCTION(BlueprintCallable, Category=Character)
    void UpdateInAirRotation(float DeltaTime);

    UFUNCTION(BlueprintCallable, Category=Character)
    bool CheckWheelOnGround(UWheeledVehicleMovementComponent* Vehicle4W);

    /** The player's maximum health. This is the highest that their health can be, and the value that their health starts at when spawned.*/
    UPROPERTY(EditDefaultsOnly, Category = "Health")
    float MaxHealth;

    /** The player's maximum fuel. This is the highest that their fuel can be, and the value that their fuel starts at when spawned.*/
    UPROPERTY(EditDefaultsOnly, Category = "Fuel")
    float MaxFuel;

    /** The player's current health. When reduced to 0, they are considered dead.*/
    UPROPERTY(ReplicatedUsing=OnRep_CurrentHealth)
    float CurrentHealth;

    /** The player's current fuel. When reduced to 0, they are considered dead.*/
    UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category=Fuel)
    float CurrentFuel;

    /** RepNotify for changes made to current health.*/
    UFUNCTION()
    void OnRep_CurrentHealth();

    /** Getter for Max Health.*/
    UFUNCTION(BlueprintPure, Category="Health")
    FORCEINLINE float GetMaxHealth() const { return MaxHealth; }

    /** Getter for Current Health.*/
    UFUNCTION(BlueprintPure, Category="Health")
    FORCEINLINE float GetCurrentHealth() const { return CurrentHealth; }

    /** Setter for Current Health. Clamps the value between 0 and MaxHealth and calls OnHealthUpdate. Should only be called on the server.*/
    UFUNCTION(BlueprintCallable, Category="Health")
    void SetCurrentHealth(float healthValue);

    /** Event for taking damage. Overridden from APawn.*/
    UFUNCTION(BlueprintCallable, Category = "Health")
    float TakeDamage( float DamageTaken, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser ) override;

    /** Getter for Max Fuel.*/
    UFUNCTION(BlueprintPure, Category="Fuel")
    FORCEINLINE float GetMaxFuel() const { return MaxFuel; }

    /** Getter for Current Fuel.*/
    UFUNCTION(BlueprintPure, Category="Fuel")
    FORCEINLINE float GetCurrentFuel() const { return CurrentFuel; }

    /** Setter for Current Fuel. Clamps the value between 0 and MaxFuel and calls OnFuelUpdate. Should only be called on the server.*/
    UFUNCTION(BlueprintCallable, Category="Fuel")
    void SetCurrentFuel(float fuelValue);

    /**
     * Check if the character can jump in the current state.
     *
     * The default implementation may be overridden or extended by implementing the custom CanJump event in Blueprints.
     *
     * @Return Whether the character can jump in the current state.
     */
    UFUNCTION(BlueprintCallable, Category=Character)
    bool CanJump();

protected:
    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Camera)
    class USpringArmComponent* SpringArm;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Camera)
    class UCameraComponent* ChaseCamera;

    void OnHealthUpdate();

    void OnFuelUpdate();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    UPROPERTY(EditAnywhere, Category = "Gameplay|Projectile")
    TSubclassOf<class ABullet> BulletClass;

    /** Delay between shots in seconds. Used to control fire rate for our test projectile, but also to prevent an overflow of server functions from binding SpawnProjectile directly to input.*/
    UPROPERTY(EditDefaultsOnly, Category="Gameplay")
    float FireRate;

    bool bIsFiringWeapon;

    UFUNCTION(BlueprintCallable, Category="Gameplay")
    void StartFire();

    UFUNCTION(BlueprintCallable, Category = "Gameplay")
    void StopFire();

    UFUNCTION(Server, Reliable)
    void HandleFire();

    FTimerHandle FiringTimer;

    UPROPERTY(EditAnywhere)
    AActor* CameraOne;

    FVector2D MovementInput;
    FVector2D CameraInput;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    /**
     * Customizable event to check if the character can jump in the current state.
     * Default implementation returns true if the character is on the ground and not crouching,
     * has a valid CharacterMovementComponent and CanEverJump() returns true.
     * Default implementation also allows for 'hold to jump higher' functionality:
     * As well as returning true when on the ground, it also returns true when GetMaxJumpTime is more
     * than zero and IsJumping returns true.
     *
     *
     * @Return Whether the character can jump in the current state.
     */
    UFUNCTION(Server, Reliable)
    void CanJumpServer();

public:

    /** Marks character as not trying to jump */
    void ResetJumpState();

    /** Event fired when the character has just started jumping */
    UFUNCTION(BlueprintNativeEvent, Category=Character)
    void OnJumped();
    virtual void OnJumped_Implementation();

    /** Update jump input state after having checked input. */
    void ClearJumpInput(float DeltaTime);

    /**
     * Get the maximum jump time for the character.
     * Note that if StopJumping() is not called before the max jump hold time is reached,
     * then the character will carry on receiving vertical velocity. Therefore it is usually
     * best to call StopJumping() when jump input has ceased (such as a button up event).
     *
     * @return Maximum jump time for the character
     */
    float GetJumpMaxHoldTime() const;

    /** When true, applying updates to network client (replaying saved moves for a locally controlled character) */
    UPROPERTY(Transient)
    uint32 bClientUpdating:1;

};
