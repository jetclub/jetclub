// Copyright Epic Games, Inc. All Rights Reserved.

#include "JetClub.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JetClub, "JetClub" );
