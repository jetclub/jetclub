// Fill out your copyright notice in the Description page of Project Settings.


#include "Freeplay.h"
#include "Kismet/GameplayStatics.h"

//bool AFreeplay::ReadyToStartMatch_Implementation()
//{
//    Super::ReadyToStartMatch();
//    return MaxNumPlayers == NumPlayers;
//}

void AFreeplay::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);
    PlayerControllerList.Add(NewPlayer);
}

void AFreeplay::BeginPlay()
{
    Super::BeginPlay();
    // 'FCString::Atoi' converts 'FString' to 'int32' and we use the static 'ParseOption' function of the
    // 'UGameplayStatics' Class to get the correct Key from the 'OptionsString'
//    MaxNumPlayers = FCString::Atoi( *(UGameplayStatics::ParseOption(OptionsString, "MaxNumPlayers"))) ;
}
