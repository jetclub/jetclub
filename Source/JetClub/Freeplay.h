// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Freeplay.generated.h"

/**
 * 
 */
UCLASS()
class JETCLUB_API AFreeplay : public AGameMode
{
	GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 MaxNumPlayers;

//    virtual bool ReadyToStartMatch() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    TArray<class APlayerController*>PlayerControllerList;

    virtual void PostLogin(APlayerController* NewPlayer) override;

protected:
    virtual void BeginPlay() override;
};
