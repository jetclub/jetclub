// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "JetState.generated.h"

/**
 * 
 */
UCLASS()
class JETCLUB_API AJetState : public APlayerState
{
	GENERATED_BODY()

public:
    virtual void PostInitializeComponents() override;

};
