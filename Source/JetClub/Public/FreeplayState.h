// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "FreeplayState.generated.h"

/**
 * 
 */
UCLASS()
class JETCLUB_API AFreeplayState : public AGameState
{
	GENERATED_BODY()

public:
    virtual void PostInitializeComponents() override;

    virtual void AddPlayerState(APlayerState* PlayerState) override;
};
