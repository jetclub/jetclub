// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "JetController.generated.h"

/**
 * 
 */
UCLASS()
class JETCLUB_API AJetController : public APlayerController
{
	GENERATED_BODY()
	
protected:
    virtual void BeginPlay() override;
};
