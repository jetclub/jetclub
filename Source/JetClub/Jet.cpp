// Fill out your copyright notice in the Description page of Project Settings.


#include "Jet.h"
#include "Math/Quat.h"
#include "Math/Rotator.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "WheeledVehicleMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Bullet.h"
#include "Net/UnrealNetwork.h"

AJet::AJet()
{
    PrimaryActorTick.bCanEverTick = true;
    SetReplicates(true);
    SetReplicateMovement(true);
    UWheeledVehicleMovementComponent* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent>(GetVehicleMovement());
    Vehicle4W->MinNormalizedTireLoad = 0.0f;
    Vehicle4W->MinNormalizedTireLoadFiltered = 0.2f;
    Vehicle4W->MaxNormalizedTireLoad = 2.0f;
    Vehicle4W->MaxNormalizedTireLoadFiltered = 2.0f;
    Vehicle4W->MaxEngineRPM = 5700.0f;

    SpringArm = CreateDefaultSubobject<USpringArmComponent> ("SpringArm");
    SpringArm->SetupAttachment(RootComponent);
    SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(-8.0f, 0.0f, 0.0f));
    SpringArm->TargetArmLength = 250.0f;
    ChaseCamera = CreateDefaultSubobject<UCameraComponent> ("ChaseCamera");
    ChaseCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
    ChaseCamera->FieldOfView = 110.f;

    MaxHealth = 100.0f;
    CurrentHealth = MaxHealth;
    MaxFuel = 200.0f;
    CurrentFuel = 100.0f;

    JumpStrength = 700.f;
    JumpKeyHoldTime = 0.0f;
    JumpMaxHoldTime = 1.5f;
    JumpMaxCount = 2;
    JumpCurrentCount = 0;
    bWasJumping = false;
    bAnyWheelTouching = true;
    bPressedJump = false;
    bCanJump = false;
    bPressedThrust = false;
    bCanThrust = false;
    bJetCam = true;

    //Initialize fire rate
    FireRate = 0.25f;
    bIsFiringWeapon = false;
}

void AJet::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    UpdateInAirRotation(DeltaTime);
    if (bPressedThrust && bCanThrust)
    {
        ApplyThrust(DeltaTime);
    }
    if (bJetCam)
    {
        FVector velDir = GetMesh()->GetPhysicsLinearVelocity();
        velDir.Z = 0;
        if (!bAnyWheelTouching && velDir.Size() > 25.0f) {
            FRotator velRot = velDir.Rotation();
            FRotator currRot = SpringArm->GetRelativeRotation();
            FRotator newRot = (velRot.Clamp() - currRot.Clamp()).GetNormalized();
            float dpt = 3.0f; // degrees per tick
            if (newRot.Yaw > dpt)
            {
                newRot.Yaw = dpt;
                SpringArm->AddRelativeRotation(newRot);
            } else if (newRot.Yaw < -dpt){
                newRot.Yaw = -dpt;
                SpringArm->AddRelativeRotation(newRot);
            }
            else{
                SpringArm->SetRelativeRotation(velRot);
            }
        }
        else {
            FVector forward = GetActorForwardVector();
            forward.Z = 0;
            FRotator currRot = SpringArm->GetRelativeRotation();
            FRotator velRot = forward.Rotation();
            FRotator newRot = (velRot.Clamp() - currRot.Clamp()).GetNormalized();
            float dpt = 6.0f; // degrees per tick
            if (newRot.Yaw > dpt)
            {
                newRot.Yaw = dpt;
                SpringArm->AddRelativeRotation(newRot);
            } else if (newRot.Yaw < -dpt) {
                newRot.Yaw = -dpt;
                SpringArm->AddRelativeRotation(newRot);
            } else {
                SpringArm->SetRelativeRotation(velRot);
                FRotator forwardRot = forward.Rotation();
                SpringArm->SetRelativeRotation(forwardRot);
            }
        }
    }
}

void AJet::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AJet::OnRep_Jump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &AJet::StopJumping);
    PlayerInputComponent->BindAction("Thrust", IE_Pressed, this, &AJet::Thrust);
    PlayerInputComponent->BindAction("Thrust", IE_Released, this, &AJet::StopThrust);
    PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &AJet::Handbrake);
    PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &AJet::ReleaseHandbrake);
    PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AJet::StartFire);
    PlayerInputComponent->BindAxis("ApplyThrottle", this, &AJet::ApplyThrottle);
    PlayerInputComponent->BindAxis("ApplySteering", this, &AJet::ApplySteering);
    PlayerInputComponent->BindAxis("Pitch", this, &AJet::Pitch);
    PlayerInputComponent->BindAxis("Roll", this, &AJet::Roll);
    PlayerInputComponent->BindAxis("PitchCamera", this, &AJet::PitchCamera);
    PlayerInputComponent->BindAxis("YawCamera", this, &AJet::YawCamera);
}

void AJet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty, TSizedDefaultAllocator<32> >&OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(AJet, CurrentHealth);
    DOREPLIFETIME(AJet, CurrentFuel);
    DOREPLIFETIME(AJet, bPressedJump);
    DOREPLIFETIME(AJet, bAnyWheelTouching);
}

void AJet::OnHealthUpdate()
{
    //Client-specific functionality
    if (IsLocallyControlled())
    {
        FString healthMessage = FString::Printf(TEXT("You now have %f health remaining."), CurrentHealth);
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, healthMessage);

        if (CurrentHealth <= 0)
        {
            FString deathMessage = FString::Printf(TEXT("You have been killed."));
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, deathMessage);
        }
    }

    //Server-specific functionality
    if (HasAuthority())
    {
        FString healthMessage = FString::Printf(TEXT("%s now has %f health remaining."), *GetFName().ToString(), CurrentHealth);
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, healthMessage);
    }

    //Functions that occur on all machines.
    /*
        Any special functionality that should occur as a result of damage or death should be placed here.
    */
}

void AJet::OnRep_CurrentHealth()
{
    OnHealthUpdate();
}

void AJet::SetCurrentHealth(float healthValue)
{
    if (HasAuthority())
    {
        CurrentHealth = FMath::Clamp(healthValue, 0.f, MaxHealth);
        OnHealthUpdate();
    }
}

void AJet::SetCurrentFuel(float fuelValue)
{
    if (HasAuthority())
    {
        CurrentFuel = FMath::Clamp(fuelValue, 0.f, MaxFuel);
    }
}

float AJet::TakeDamage(float DamageTaken, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    float damageApplied = CurrentHealth - DamageTaken;
    SetCurrentHealth(damageApplied);
    return damageApplied;
}

void AJet::StartFire()
{
    if (!bIsFiringWeapon)
    {
        bIsFiringWeapon = true;
        UWorld* World = GetWorld();
        World->GetTimerManager().SetTimer(FiringTimer, this, &AJet::StopFire, FireRate, false);
        HandleFire();
    }
}

void AJet::StopFire()
{
    bIsFiringWeapon = false;
}

void AJet::HandleFire_Implementation()
{
    FRotator spawnRotation = GetMesh()->GetComponentRotation();
    FVector spawnLocation = GetActorLocation() + ( spawnRotation.Vector()  * 250.0f ) + (GetActorUpVector() * 70.0f);

    FActorSpawnParameters spawnParameters;
    spawnParameters.Instigator = GetInstigator();
    spawnParameters.Owner = this;

    ABullet* spawnedProjectile = GetWorld()->SpawnActor<ABullet>(BulletClass, spawnLocation, spawnRotation, spawnParameters);
}

void AJet::ApplyThrottle(float AxisValue)
{
    GetVehicleMovementComponent()->SetThrottleInput(AxisValue);
}

void AJet::ApplySteering(float AxisValue)
{
    GetVehicleMovementComponent()->SetSteeringInput(AxisValue);
}

void AJet::Pitch(float AxisValue)
{
    if (AxisValue != 0.f)
    {
        AddControllerPitchInput(AxisValue);
    }
}

void AJet::Roll(float AxisValue)
{
    if (AxisValue != 0.f)
    {
        AddControllerRollInput(AxisValue);
    }
}

void AJet::PitchCamera(float AxisValue)
{
    if (AxisValue != 0.f)
    {
        AddControllerPitchInput(AxisValue);
    }
}

void AJet::YawCamera(float AxisValue)
{
    if (AxisValue != 0.f)
    {
        AddControllerYawInput(AxisValue);
    }
}

void AJet::StopJumping()
{
    UE_LOG(LogTemp, Warning, TEXT("RELEASE JUMP"));
    bPressedJump = false;
    ResetJumpState();
}

void AJet::JumpServer_Implementation()
{
    OnRep_Jump();
}

void AJet::ApplyThrust(float DeltaTime)
{
    if (CurrentFuel > 0)
    {
        bCanThrust = true;
        if (UWheeledVehicleMovementComponent* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent>(GetVehicleMovement()))
        {
            if (UPrimitiveComponent* VehicleMesh = Vehicle4W->UpdatedPrimitive)
            {
                FVector Thrust = GetActorForwardVector() * 30.0f;
                VehicleMesh->AddImpulse(Thrust, NAME_None, true);
                CurrentFuel -= DeltaTime * 20;
                UE_LOG(LogTemp, Warning, TEXT("The float value is: %f"), CurrentFuel);
            }
        }
        return;
    }
    bCanThrust = false;
}
void AJet::Thrust()
{
    bPressedThrust = true;
    if (CurrentFuel > 0)
    {
        bCanThrust = true;
    }
}
void AJet::StopThrust()
{
    bPressedThrust = false;
}

void AJet::OnRep_Jump()
{
    UE_LOG(LogTemp, Warning, TEXT("Pressed JUMP"));
    bPressedJump = true;
    if (!HasAuthority()){
        JumpServer();
        return;
    }
    bCanJump = CanJump();
    if (bCanJump)
    {
        FVector JumpVelocity = GetActorUpVector() * 750.0f;
        if (UWheeledVehicleMovementComponent* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent>(GetVehicleMovement()))
        {
            if (UPrimitiveComponent* VehicleMesh = Vehicle4W->UpdatedPrimitive)
            {
                VehicleMesh->SetAllPhysicsLinearVelocity(JumpVelocity, true);
            }
        }
        JumpCurrentCount++;
    }
}

void AJet::ReleaseHandbrake()
{
    GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AJet::Handbrake()
{
    GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AJet::ResetJumpState()
{
    bPressedJump = false;
    bWasJumping = false;
    JumpKeyHoldTime = 0.0f;

    if (GetVehicleMovement() && !GetVehicleMovement()->IsFalling())
    {
        JumpCurrentCount = 0;
    }
}

void AJet::CanJumpServer_Implementation()
{
    CanJump();
}

void AJet::OnJumped_Implementation()
{
}

void AJet::ClearJumpInput(float DeltaTime)
{
    if (bPressedJump)
    {
        JumpKeyHoldTime += DeltaTime;

        // Don't disable bPressedJump right away if it's still held.
        // Don't modify JumpForceTimeRemaining because a frame of update may be remaining.
        if (JumpKeyHoldTime >= GetJumpMaxHoldTime())
        {
            bPressedJump = false;
        }
    }
    else
    {
        JumpForceTimeRemaining = 0.0f;
        bWasJumping = false;
    }
}

float AJet::GetJumpMaxHoldTime() const
{
    return JumpMaxHoldTime;
}

bool AJet::CanJump()
{
    if (!HasAuthority()){
        CanJumpServer();
    }
    // Ensure JumpHoldTime and JumpCount are valid.
    if (UWheeledVehicleMovementComponent* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent>(GetVehicleMovement()))
    {
        if (!bWasJumping || GetJumpMaxHoldTime() <= 0.0f || CheckWheelOnGround(Vehicle4W))
        {
            if (JumpCurrentCount == 0 && GetVehicleMovement()->IsFalling())
            {
                bCanJump = JumpCurrentCount + 1 < JumpMaxCount;
            }
            else
            {
                bCanJump = JumpCurrentCount < JumpMaxCount;
            }
        }
        else
        {
            // Only consider JumpKeyHoldTime as long as:
            // A) The jump limit hasn't been met OR
            // B) The jump limit has been met AND we were already jumping
            const bool bJumpKeyHeld = (bPressedJump && JumpKeyHoldTime < GetJumpMaxHoldTime());
            bCanJump = bJumpKeyHeld &&
                        ((JumpCurrentCount < JumpMaxCount) || (bWasJumping && JumpCurrentCount == JumpMaxCount));
        }
    }

    return bCanJump;
}

bool AJet::CheckWheelOnGround(UWheeledVehicleMovementComponent* Vehicle4W)
{
    bAnyWheelTouching = false;
    JumpCurrentCount = 0;
    TArray<class UVehicleWheel*> wheels = Vehicle4W->Wheels;
    for (auto& wheel : wheels)
    {
        if (!wheel->IsInAir())
        {
        bAnyWheelTouching = true;
        break;
        }
    }
    return bAnyWheelTouching;
}

void AJet::UpdateInAirRotation(float DeltaTime)
{
    if (UWheeledVehicleMovementComponent* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent>(GetVehicleMovement()))
    {
        CheckWheelOnGround(Vehicle4W);
        if (!bAnyWheelTouching && InputComponent)
        {
            const float PitchInput = InputComponent->GetAxisValue("Pitch");
            const float YawInput = InputComponent->GetAxisValue("ApplySteering");
            const float RollInput = InputComponent->GetAxisValue("Roll");

            if (UPrimitiveComponent* VehicleMesh = Vehicle4W->UpdatedPrimitive)
            {
                VehicleMesh->SetAngularDamping(5.0f);
                const FVector MovementVector = FVector(RollInput, PitchInput, YawInput) * DeltaTime * 3200.f;;
                const FVector NewAngularMovement = GetActorRotation().RotateVector(MovementVector);
                VehicleMesh->SetPhysicsMaxAngularVelocityInDegrees(360.0f);
                VehicleMesh->SetPhysicsAngularVelocityInDegrees(NewAngularMovement, true);
            }
        }
    }
}
