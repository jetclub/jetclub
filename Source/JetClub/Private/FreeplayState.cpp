// Fill out your copyright notice in the Description page of Project Settings.

#include "FreeplayState.h"
#include "GameFramework/PlayerState.h"

void AFreeplayState::PostInitializeComponents()
{
    Super::PostInitializeComponents();
    UWorld* World = GetWorld();
    UE_LOG(LogTemp, Warning, TEXT("FreeplayState post init"));
//    for(TActorIterator<APlayerState> It(World); It; ++It)
//    {
//        AddPlayerState(*It);
//    }
}

void AFreeplayState::AddPlayerState(APlayerState* PlayerState)
{
    Super::AddPlayerState(PlayerState);
}
